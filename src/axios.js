import axios from 'axios'
import cookie from 'js-cookie'
import router from './router';
import errCode from './errCode'
import { Notification } from 'element-ui';

axios.defaults.timeout = 5000

axios.interceptors.request.use((config) => {
    let token = cookie.get('token')
    if (config.url !== '/user/login') {
        if (!token) {
            router.push('/login')
        }
        config.headers = {
            Authorization: token
        }
    }
    return config
})

axios.interceptors.response.use(response => {
    if (response.status >= 200 && response.status < 300) {
        return response
    }
    if (response.status === 404) {
        router.push('/error')
    }
    Notification.error({
        title: '错误',
        message: errCode[response.status]
    })
    return response
}, (err) => {
    console.log(err)
    Notification.error({
        title: `服务器错误！`
    })
    return Promise.reject(err)
})


export default axios;
