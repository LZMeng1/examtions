export default [
    {
        path: '/home',
        name: 'main',
        redirect: '/home/home',
        meta: ['管理员', '出题者', '浏览者'],
        component: () => import('../../views/main.vue'),
        children: [
            {
                tit: '试题管理',
                icon: 'el-icon-receiving',
                path: 'home',
                name: 'home',
                component: () => import('../../components/Home'),
                meta: ['管理员', '出题者'],
                children: [
                    {
                        tit: '查看试题',
                        path: 'examine',
                        name: 'examine',
                        meta: ['管理员', '出题者'],
                        component: () => import('@/views/questions/examine')
                    }, {
                        tit: '添加试题',
                        path: 'addQuestions',
                        name: 'addQuestions',
                        meta: ['出题者'],
                        component: () => import('@/views/questions/addQuestions')
                    }, {
                        tit: '试题分类',
                        path: 'questionsFy',
                        name: 'questionsFy',
                        meta: ['管理员', '出题者'],
                        component: () => import('@/views/questions/questionsFy')
                    },
                    {
                        path: 'editQuestions',
                        name: 'editQuestions',
                        meta: ['出题者'],
                        component: () => import('@/views/questions/editQuestions')
                    }
                ]
            },
            {
                tit: '用户管理',
                icon: 'el-icon-user',
                path: 'home',
                name: 'home',
                component: () => import('../../components/Home'),
                meta: ['管理员'],
                children: [
                    {
                        tit: '添加用户',
                        path: 'addUser',
                        name: 'addUser',
                        meta: ['管理员'],
                        component: () => import('@/views/users/addUser')
                    }, {
                        tit: '用户展示',
                        path: 'exhibition',
                        name: 'exhibition',
                        meta: ['管理员'],
                        component: () => import('@/views/users/exhibition')
                    }
                ]
            },
            {
                tit: '考试管理',
                icon: 'el-icon-collection',
                meta: ['管理员'],
                path: 'home',
                name: 'home',
                component: () => import('../../components/Home'),
                children: [
                    {
                        tit: '添加考试',
                        path: 'addExam',
                        name: 'addExam',
                        meta: ['管理员'],
                        component: () => import('@/views/examination/addExam')
                    }, {
                        tit: '试卷列表',
                        path: 'examList',
                        name: 'examList',
                        meta: ['管理员'],
                        component: () => import('@/views/examination/examList')
                    },
                    {
                        path: 'detail',
                        name: 'detail/:data',
                        meta: ['管理员'],
                        component: () => import('../../views/questions/detail/index.vue')
                    },
                    {
                        path: 'examList/:id',
                        name: 'detail',
                        meta: ['管理员'],
                        component: () => import('@/views/examination/detail')
                    },
                    {
                        path: 'addExamDetail',
                        name: 'addExamDetail',
                        meta: ['管理员'],
                        component: () => import('@/views/examination/addExamDetail')
                    }
                ]
            },
            {
                tit: '班级管理',
                icon: 'el-icon-thumb',
                meta: ['管理员'],
                path: 'home',
                name: 'home',
                component: () => import('../../components/Home'),
                children: [
                    {
                        tit: '学生管理',
                        path: 'study',
                        name: 'study',
                        meta: ['管理员'],
                        component: () => import('@/views/classes/study')
                    }, {
                        tit: '班级管理',
                        path: 'classFy',
                        name: 'classFy',
                        meta: ['管理员'],
                        component: () => import('@/views/classes/classFy')
                    }, {
                        tit: '教室管理',
                        path: 'grades',
                        name: 'grades',
                        meta: ['管理员'],
                        component: () => import('@/views/classes/grades')
                    }
                ]
            },
            {
                tit: '阅卷管理',
                icon: 'el-icon-wallet',
                meta: ['管理员', '出题者'],
                path: 'home',
                name: 'home',
                component: () => import('../../components/Home'),
                children: [
                    {
                        tit: '待批班级',
                        path: 'waitMarking',
                        name: 'waitMarking',
                        meta: ['管理员', '出题者'],
                        component: () => import('@/views/marking/waitMarking')
                    },
                    {
                        path: 'waitDetail/:id',
                        name: 'waitDetail',
                        meta: ['管理员', '出题者'],
                        component: () => import('@/views/marking/classmateDetail')
                    },
                    {
                        path: 'classmate',
                        name: 'classmate',
                        meta: ['管理员', '出题者'],
                        component: () => import('@/views/marking/classmate')
                    }
                ]
            }
        ]
    }
]
