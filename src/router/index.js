import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
Vue.use(Router)

let routers = new Router({
    routes: [
        {
            path: '/',
            redirect: '/login'
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('../views/login')
        },
        {
            path: '/error',
            component: () => import('../views/error')
        }
    ]
})
export default routers
routers.beforeEach((to, from, next) => {
    if (to.path === '/login') {
        next()
    } else {
        store.dispatch('getList')
        next()
    }
})


