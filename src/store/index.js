import vue from 'vue'
import Vuex from 'vuex'
import addExam from './modules/examination/addExam'
import examine from './modules/questions/examine'
import waitMarking from './modules/marking/waitMarking'
import axios from 'axios'
import routeList from '@/router/routes'
import { filteRouter } from '@/utils'
import router from '@/router'

vue.use(Vuex)

export default new Vuex.Store({
    state: {
        list: []
    },
    mutations: {
        setList (state, data) {
            console.log(data)
            router.addRoutes(data)
            state.list = data
        }
    },
    actions: {
        getList ({commit}) {
            axios.get('/api/user/userInfo')
                .then(({ data: { data } }) => {
                    commit('setList', filteRouter(routeList, data.identity_text))
                })
        }
    },
    getters: {
        list (state) {
            return state.list
        }
    },
    modules: {
        addExam,
        examine,
        waitMarking
    }
})
