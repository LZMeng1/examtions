import axios from '../../../axios'
export default {
    state: {
        typeList: [], // 考试类型
        courseList: [], // 课程
        list: [], // 试卷列表,
        examList: [] // 所有的试题
    },
    getters: {
        getsType (state) {
            return state.typeList
        },
        getsCourse (state) {
            return state.courseList
        },
        getsList (state) {
            return state.list
        },
        getsExamList (state) {
            return state.examList
        }
    },
    mutations: {
        getTypes (state, data) {
            state.typeList = data
        },
        getCourses (state, data) {
            state.courseList = data
        },
        getLists (state, data) {
            state.list = data
        },
        getExamLists (state, data) {
            state.examList = data
        }
    },
    actions: {
        getType ({commit}) {
            axios.get('/api/exam/examType').then(res => {
                commit('getTypes', res.data.data)
            })
        },
        getCourse ({commit}) {
            axios.get('/api/exam/subject').then(res => {
                commit('getCourses', res.data.data)
            })
        },
        getList ({commit}) {
            axios.get('/api/exam/exam').then(res => {
                commit('getLists', res.data.exam)
            })
        },
        getExamList ({commit}) {
            axios.get('/api/exam/questions/new').then(res => {
                commit('getExamLists', res.data.data)
            })
        }
    }
}
