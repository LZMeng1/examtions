export default {
    state: {
        flag: false
    },
    getters: {
        dialogFormVisible (state) {
            return state.flag
        }
    },
    mutations: {
        setFlag (state, flag) {
            state.flag = flag
        }
    }
}
