import axios from '../../../axios'
export default {
    state: {
        questions_new_data: [],
        getQuestionsType: [],
        examType: [],
        subjecttyp: [],
        questions_list: []
    },
    mutations: {
        updatadata (state, data) {
            console.log(data)
            state.questions_new_data = data
        },
        updatagetQuestionsType (state, data) {
            // console.log(data) // 试题类型
            state.getQuestionsType = data
        },
        updatagetexamType (state, data) { // 考试类型
            // console.log(data)
            state.examType = data
        },
        updatasubject (state, data) {
            state.subjecttyp = data
        },
        getlist (state, data) { // 按出题者的不同，分块渲染
            state.questions_list = []
            data.data.forEach((item) => {
                let oid = state.questions_list.findIndex((ite) => { // 去重
                    return ite.user === item.user_name
                })
                if (oid === -1) {
                    let arr = data.data.filter((it) => {
                        return it.user_name === item.user_name
                    })
                    state.questions_list.push({user: item.user_name, list: arr}) // 将数据和出题者--组合成对象
                }
            })
        }
    },
    actions: {
        getData ({commit}) {
            axios.get('/api/exam/questions/new').then((data) => {
                commit('updatadata', data.data)
                commit('getlist', data.data)
            })
        },
        gettype ({commit}) {
            axios.get('/api/exam/getQuestionsType').then((data) => {
                commit('updatagetQuestionsType', data.data)
            });
        },
        getexamType ({commit}) {
            axios.get('/api/exam/examType').then((data) => {
                commit('updatagetexamType', data.data)
            });
        },
        getsubject ({commit}) {
            axios.get('/api/exam/subject').then((data) => {
                commit('updatasubject', data.data)
            })
        },
        onSubmit ({state, commit}, objid) { // 提交 按条件筛选数据
            // console.log(objid.subarr)// 课程类型数组
            let regioid = state.examType.data.filter((item) => item.exam_name === objid.region)
            // console.log(regioid[0].exam_id)// 考试类型ID
            let regionid = state.getQuestionsType.data.filter((item) => item.questions_type_text === objid.regio)
            // console.log(regionid[0].questions_type_id)// 题目类型ID
            if (objid.subarr.subject_id !== '' && regioid[0].exam_id !== '' && regionid[0].questions_type_id !== '') {
                axios.get('/api/exam/questions/condition', {params: {subject_id: objid.subarr.subject_id, exam_id: regioid[0].exam_id, questions_type_id: regionid[0].questions_type_id}}).then((data) => {
                    commit('getlist', data.data)
                })
            }
        },
        changegetQuestionsType ({state}, val) {
            axios.get('/api/exam/insertQuestionsType', {params: {'text': val, 'sort': ++state.getQuestionsType.data.length}}).then(() => {
            })
        }
    },
    getters: {
        resdata (state) {
            return state.questions_new_data
        },
        resgetQuestionsType (state) {
            return state.getQuestionsType
        },
        resexamType (state) {
            return state.examType
        },
        ressubject (state) {
            return state.subjecttyp
        },
        reslist (state) {
            return state.questions_list
        }
    }
}
