import axios from '../../../axios'
export default {
    state: {
        classList: [],
        classmateList: []
    },
    getters: {
        getsClassList (state) {
            return state.classList
        },
        getsClassmate (state) {
            return state.classmateList
        }
    },
    mutations: {
        getClassLists (state, data) {
            state.classList = data
        },
        getClassmates (state, data) {
            if (data) {
                state.classmateList = data
            }
        },
        clearArr (state) {
            state.classmateList = []
        }
    },
    actions: {
        getClassList ({commit}) { // 获取所有已经分配教室的班级
            axios.get('/api/manger/grade').then(res => {
                commit('getClassLists', res.data.data)
            })
        },
        getClassmate ({commit}, id) { // 获取学生列表
            axios.get('/api/exam/student', {
                params: {
                    grade_id: id
                }
            }).then(res => {
                console.log(res.data)
                commit('getClassmates', res.data.exam)
            })
        }
    }
}
