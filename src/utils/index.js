// 筛选数组
let filteList = (allList, ...opt) => {
    let list = opt.filter(item => item[1])
    return allList.filter(item => (
        list.every(val => item[val[0]] === val[1])
    ))
}
// 深拷贝
// function deepCopy(obj) {
//     // 创建一个新对象
//     let result = obj.constructor === Object ? {} : []
//     // eslint-disable-next-line one-var
//     let keys = Object.keys(obj),
//         key = null,
//         temp = null;

//     for (let i = 0; i < keys.length; i++) {
//         key = keys[i];
//         temp = obj[key];
//         // 如果字段的值也是一个对象则递归操作
//         if (temp && obj.constructor === Object) {
//             result[key] = deepCopy(temp);
//         } else {
//             // 否则直接赋值给新对象
//             result[key] = temp;
//         }
//     }
//     return result;
// }
// 过滤路由
let filteRouter = (routeList, other) => {
    // let arr = deepCopy(routeList)
    let list = routeList.filter(item => {
        if (item.meta) {
            return item.meta.findIndex(v => v === other) !== -1
        }
    })
    list.forEach(val => {
        if (val.children) {
            val.children = filteRouter(val.children, other)
        }
    })
    return list
}

// eslint-disable-next-line standard/object-curly-even-spacing
export { filteList, filteRouter}
